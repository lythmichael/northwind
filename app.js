//webserver
const express = require('express')
const app = express()

//Parsa skickade data från client body('post')
const bodyParser = require('body-parser')
const path = require('path')

//hantera inkommande data som json
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))

// Hitta index.html och andra public files
app.use(express.static(path.join(__dirname, 'public')));

//Ladda in modul för sql serveråtkomst
const sql = require('msnodesqlv8');

//Connection sträng som vanligt
const connString = "server=.;Database=northwind;Trusted_Connection=Yes;Driver={SQL Server Native Client 11.0}";

//Stored proc GetCustomers
app.get('/me', function (req, res) {
    const query = "exec GetCustomers  @customerid='" + 'ALFKI' + "';"
    console.log(query)
    sql.query(connString, query, (err, rows) => {
        console.log(rows);
        res.json(rows)
    })

});

app.get('/mySql/:s', function (req, res) {
    const myQuery = req.params.s
    sql.query(connString, myQuery, (err, rows) => {

        res.json(rows)
    })
});
//Alla data hamnar i req.body se rad 10 och 11
app.post('/newCustomer', function (req, res) {
    var body = req.body
    console.log(body)
    var customerID = req.body.customerID
    var companyName=req.body.companyName
    var contactName = req.body.contactName
    var contactTitle=req.body.contactTitle
    var address = req.body.address
    var city=req.body.city
    var region = req.body.region
    var postalCode=req.body.postalCode
    var country = req.body.country
    var phone=req.body.phone
    var fax = req.body.fax
    
    insertCustomer(customerID, companyName, contactName,
        contactTitle, address, city, region, postalCode,
        country, phone, fax)
    
})
function insertCustomer(customerID, companyName, contactName,
    contactTitle, address, city, region, postalCode,
    country, phone, fax)
{
    var myQuery = `insert into customers(customerID, companyName, contactName,
        contactTitle, address, city, region, postalCode,
        country, phone, fax)values

    ('${customerID}','${companyName}','${contactName}'
    ,'${contactTitle}','${address}','${city}'
    ,'${region}','${postalCode}','${country}'
    ,'${phone}','${fax}')`
    sql.query(connString, myQuery, (err, rows) => {
        if(err) console.log(err)
        
    })
}

function insertAlbum(artistid,title,låtar)
{
    var myQuery = `insert into album(artistid,title)values

    (${artistid},'${title}')`
    sql.query(connString, myQuery, (err, rows) => {
        if(err) console.log(err)
        getAlbumid(låtar)
    })
}
     
function getAlbumid(låtar)
{
 //Hämta senaste albumid
 myQuery = `select max(albumid) from album`
 sql.query(connString, myQuery, (err, rows) => {
    if(err) console.log(err)
     var albumid = rows[0].Column0
     insertTrack(albumid,låtar)
 })
}

 //Insert alla låtar OBS sätt mediatypeid till 1
function insertTrack(albumid, låtar)
{
låtar.forEach(name => {
    myQuery = `insert into track(albumid,name,mediatypeid,
        milliseconds,unitprice)values(${albumid},'${name}',1,0,0)`
    sql.query(connString, myQuery, (err, rows) => {
    if(err) console.log(err)
    });
})
}

app.listen(9000)
console.log("lyssnar på port 9000")
